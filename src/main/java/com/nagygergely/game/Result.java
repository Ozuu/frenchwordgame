package com.nagygergely.game;

/**
 * The Result class is tracking your correct answers for showing your result in the end of the game.
 */

public class Result {
    private int correctAnswer = 0;
    private int questionsNumber;

    public Result(int questionsNumber) {
        this.questionsNumber = questionsNumber;
    }

    public void printResult() {
        String yourResult = """
                  Congratulation your score is:
                         %1s / %2s""";
        System.out.println(String.format(yourResult,correctAnswer,questionsNumber));

        if (correctAnswer <= questionsNumber * 0.5) {
            System.out.println("Well, this didn't go well.");
        } else if (correctAnswer <= questionsNumber * 0.80 && correctAnswer >= questionsNumber * 0.5) {
            System.out.println("Your score is average.");
        } else{
            System.out.println("You are smart as hell.");
        }
    }

    public void increasingCorrectAnswers(){
        correctAnswer++;
    }
}
