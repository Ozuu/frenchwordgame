package com.nagygergely.game;

import com.nagygergely.game_data_handler.WordsStore;
import com.nagygergely.user_input.AnswerInputReader;

import java.util.ArrayList;
import java.util.List;

/**
 * This class generates questions from the available data.
 */

public class Question {
    private List<String> answerOptions;
    private List<String> questionChar = List.of("a","b","c","d");
    private List<Integer> questionNumber = List.of(1, 2, 3, 4);
    private int letterIndex = 0;
    private AnswerInputReader inputReader;
    private QuestionEvaluator evaluator;

    public Question() {
        this.answerOptions = new ArrayList<>();
        this.inputReader = new AnswerInputReader();
        this.evaluator = new QuestionEvaluator();
    }

    public void createQuestion(int currentQuestionNumber, WordsStore words, RandomOptionPicker randomPick) {
        int answerPosition = randomPick.randomNumber(questionNumber);
        String hungarianWord = randomPick.randomKey(words);
        String answer = words.getValue(hungarianWord);

        for (int i = 0; i < 4; i++) {
            if (i == answerPosition) {
                answerOptions.add(answer);
            } else {
                String option = randomPick.randomValue(words);
                answerOptions.add(option);
            }
        }

        printQuestionText(currentQuestionNumber, hungarianWord);

        String yourGuess = inputReader.readInput();

        evaluator.evaluateAnswer(answer,yourGuess,answerOptions,questionChar);
    }

    private void printQuestionText(int currentQuestionNumber, String hungarianWord) {
        System.out.println("\n" + (currentQuestionNumber + 1) + ") What is the meaning of " + "'" + hungarianWord + "'" + " in french?");
        for (String option : answerOptions) {
            System.out.println("\t" + questionChar.get(letterIndex) + ", " + option);
            letterIndex++;
        }
    }

    public boolean isRight() {
        return evaluator.isRight();
    }
}
