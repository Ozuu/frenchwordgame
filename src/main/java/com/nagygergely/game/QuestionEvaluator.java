package com.nagygergely.game;

import java.util.List;

public class QuestionEvaluator {
    private boolean isRight = false;

    public void evaluateAnswer(String answer, String yourGuess, List<String> answerOptions, List<String> questionChar) {
        int answerIndex = answerOptions.indexOf(answer);
        int yourGuessIndex = questionChar.indexOf(yourGuess);
        String answerChar = questionChar.get(answerIndex);

        if (answerIndex == yourGuessIndex) {
            System.out.println("Right! Good job!");
            isRight = true;
        } else {
            System.out.println("Sorry, the right answer is " + "'" + answerChar + "'.");
        }
    }

    public boolean isRight() {
        return isRight;
    }
}
