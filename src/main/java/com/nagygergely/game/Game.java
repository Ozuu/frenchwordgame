package com.nagygergely.game;

import com.nagygergely.game_data_handler.GameDataLoader;
import com.nagygergely.settings.SettingsHolder;
import com.nagygergely.settings.SettingsLoader;
import com.nagygergely.game_data_handler.WordsStore;


/**
 * This class is the game itself. It has one method which generates the questions.
 * For now, the constructor's parameter decides the number of questions.
 */

public class Game {
    private int numberOfQuestions;
    private WordsStore data;
    private RandomOptionPicker randomPicker;
    private SettingsHolder settings = SettingsLoader.loadSettings();
    private GameDataLoader gameDataLoader = new GameDataLoader();

    public Game() {
        this.numberOfQuestions = settings.getNumberOfQuestions();
        this.randomPicker = new RandomOptionPicker();
        this.data = gameDataLoader.loadData(settings.getTopic());
    }

    public void start() {
        Result yourResult = new Result(numberOfQuestions);

        for (int i = 0; i < numberOfQuestions; i++) {
            Question question = new Question();
            question.createQuestion(i, data, randomPicker);

            if (question.isRight()) {
                yourResult.increasingCorrectAnswers();
            }
        }

        yourResult.printResult();
    }
}
