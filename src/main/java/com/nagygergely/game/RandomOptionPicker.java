package com.nagygergely.game;

import com.nagygergely.game_data_handler.WordsStore;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The main purpose of this class is to pick a random key (hungarian word) or value (french word). This key will be
 * the word whose meaning will be chosen by the player.
 */

public class RandomOptionPicker {
    private Random  random = new Random();

    public String randomKey(WordsStore data){
        List<String> temporaryKeySet = new ArrayList<>(data.getWordsStore().keySet());
        int randomIndex = random.nextInt(temporaryKeySet.size());
        return temporaryKeySet.get(randomIndex);
    }

    public String randomValue(WordsStore data){
        String key = randomKey(data);
        return data.getValue(key);
    }

    public int randomNumber(List<Integer> numbers){
        int number = random.nextInt(numbers.size());
        return number;
    }
}
