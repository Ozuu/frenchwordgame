package com.nagygergely.main;

import com.nagygergely.settings.SettingsHolder;
import com.nagygergely.settings.SettingsLoader;
import com.nagygergely.game.Game;
import com.nagygergely.menu.Menu;

import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {
        startGame();
    }

    private static void startGame(){
        Scanner inputScanner = new Scanner(System.in);
        Game myGame = new Game();
        SettingsHolder settings = SettingsLoader.loadSettings();

        Menu gameMenu = new Menu(inputScanner, myGame,settings);
        try{
            gameMenu.startMenu();
        } catch (InterruptedException e){
            System.out.println("Whoops... something went wrong. The program exits");
        }
    }
}
