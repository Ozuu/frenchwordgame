package com.nagygergely.user_input;

/**
 * This class is validating answer's input.
 * We only accept answers if it is one of the available answer options.
 */

public class AnswerInputChecker implements InputChecker {

    @Override
    public boolean validInput(String symbol) {
        return symbol.equals("a") || symbol.equals("b") || symbol.equals("c") || symbol.equals("d");
    }
}
