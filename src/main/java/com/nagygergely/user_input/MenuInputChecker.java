package com.nagygergely.user_input;

public class MenuInputChecker implements InputChecker {
    private int optionsNumber;

    public MenuInputChecker(int optionsNumber) {
        this.optionsNumber = optionsNumber;
    }

    @Override
    public boolean validInput(String symbol) {
        String rangePattern = String.format("[1-%s]", optionsNumber);
        return symbol.matches(rangePattern);
    }
}
