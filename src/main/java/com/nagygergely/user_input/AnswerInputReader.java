package com.nagygergely.user_input;

import java.util.Scanner;

/**
 * Read the answer's input which always must be valid.
 */

public class AnswerInputReader {
    private Scanner scanner = new Scanner(System.in);
    InputChecker checker = new AnswerInputChecker();

    public String readInput() {
        boolean invalidInput = true;
        System.out.println("\nPlease enter the letter of your guess: ");
        String answer = scanner.nextLine();

        while (invalidInput) {
            if (!checker.validInput(answer)) {
                System.out.println("You probably misspelled your answer. Try again.");
                answer = scanner.nextLine();
            } else {
                invalidInput = false;
            }
        }
        return answer;
    }
}
