package com.nagygergely.user_input;

public interface InputChecker {
    boolean validInput(String symbol);
}
