package com.nagygergely.settings;

import java.util.ArrayList;

/**
This holder will contains the settings which can be modifiable. The order is important in this case:
 Index 0 : Number of questions.
 Index 1 : Topic of words
 */

public class SettingsHolder {
    private ArrayList<String> settings;

    public SettingsHolder(ArrayList<String> settings) {
        this.settings = settings;
    }

    public void printSettings(){
        String settingsProperties = """
                 Settings:
                     Number of questions: %1s
                     Topic: %2s
                 """;
        System.out.println(String.format(settingsProperties,getNumberOfQuestions(),getTopic()));
    }

    public int getNumberOfQuestions(){
        return Integer.parseInt(settings.get(0));
    }

    public String getTopic(){
        return settings.get(1);
    }
}
