package com.nagygergely.settings;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class SettingsLoader {

    public static SettingsHolder loadSettings() {
        ArrayList<String> settings = new ArrayList<>(2);

        try(BufferedReader reader = new BufferedReader(new FileReader("settings.txt"))){
            String wholeLine = reader.readLine();
            String[] settingsElements = wholeLine.split(" - ");
            String numberOfQuestionsInString = settingsElements[0];
            String questionTopic = settingsElements[1];
            settings.add(numberOfQuestionsInString);
            settings.add(questionTopic);
        } catch(IOException e){
            e.printStackTrace();
        }

        return new SettingsHolder(settings);
    }
}
