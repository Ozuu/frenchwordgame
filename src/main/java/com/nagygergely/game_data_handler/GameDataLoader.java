package com.nagygergely.game_data_handler;

import java.sql.*;

import java.util.HashMap;
import java.util.Map;

/**
 * The main purpose of this class to read and store data from any txt file given as a parameter.
 *
 * The return will change depend on the container class's type which will be filled with data.
 */


public class GameDataLoader {
    private static final String DB_URL = "jdbc:postgresql://localhost/wordGameData";
    private static final String USER = "Ozuu";
    private static final String PASS = "UTEq7H";


    public WordsStore loadData(String fileName) {
        Map<String, String> words = new HashMap<>();

        try(Connection conn = DriverManager.getConnection(DB_URL,USER,PASS);
            Statement stmt = conn.createStatement()) {
            Class.forName("org.postgresql.Driver");

            String sql = "SELECT french_word, english_word FROM " + fileName;
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String french = rs.getString("french_word");
                String english = rs.getString("english_word");
                words.put(french,english);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new WordsStore(words);
    }
}
