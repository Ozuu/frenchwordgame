package com.nagygergely.game_data_handler;

import java.util.Collections;
import java.util.Map;

public class WordsStore {
    private Map<String,String> dataContainer;

    public WordsStore(Map<String, String> dataContainer) {
        this.dataContainer = dataContainer;
    }

    public Map<String, String> getWordsStore() {
        return Collections.unmodifiableMap(dataContainer);
    }

    public String getValue(String key){
        return getWordsStore().get(key);
    }
}
