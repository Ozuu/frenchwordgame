package com.nagygergely.menu;

import com.nagygergely.user_input.InputChecker;
import com.nagygergely.user_input.MenuInputChecker;
import com.nagygergely.settings.SettingsHolder;
import com.nagygergely.game.Game;

import java.util.Scanner;

public class Menu {
    private Scanner inputScanner;
    private Game wordGame;
    private SettingsHolder settings;
    private InputChecker inputChecker;

    public Menu(Scanner inputScanner, Game game, SettingsHolder settings) {
        this.inputScanner = inputScanner;
        this.wordGame = game;
        this.settings = settings;
    }

    public void startMenu() throws InterruptedException {
        System.out.print("This is");
        Thread.sleep(1000);
        System.out.print(" the big");
        Thread.sleep(1000);
        System.out.print(" French World Game!\n");
        Thread.sleep(1000);

        launchMainMenu();
    }

    private void launchMainMenu(){
        boolean noQuitCommand = true;

        while (noQuitCommand) {
            String menuBlock = """
             Menu
               1. Start Game
               2. Settings
               3. Quit""";

            this.inputChecker = new MenuInputChecker(3);
            System.out.println(menuBlock);
            int menuChoice = getInput();

            switch (menuChoice) {
                case 1 -> wordGame.start();
                case 2 -> launchSettingsMenu();
                case 3 -> noQuitCommand = quit();
            }
        }
    }

    private void launchSettingsMenu() {
        this.inputChecker = new MenuInputChecker(2);
        String settingsMenu = """
               1. Print Settings
               2. Change Settings
               """;
        System.out.println(settingsMenu);
        int settingsChoice = getInput();

        switch (settingsChoice) {
            case 1 -> settings.printSettings();
            case 2 -> System.out.println("Modifying settings...");
        }
    }

    private int getInput() {
        boolean isExistingChoice = false;
        int choice = 0;
        while (!isExistingChoice) {
            System.out.println("Please enter the number of your choice: ");
            String input = inputScanner.nextLine();
            if (inputChecker.validInput(input)) {
                choice = Integer.parseInt(input);
                isExistingChoice = true;
            } else {
                System.out.println("Whoops. We need a correct number here.");
            }
        }
        return choice;
    }

    private boolean quit() {
        System.out.println("Goodbye!");
        return false;
    }
}
